<?php

/**
 * This is the model class for table "bikerentral".
 *
 * The followings are the available columns in table 'bikerentral':
 * @property integer $RFID
 * @property integer $BikeID
 * @property integer $SrcStationID
 * @property integer $DesStationID
 * @property string $StartTime
 * @property string $EndTiem
 * @property integer $Cost
 * @property integer $BikeRentral
 * @property integer $UserId
 *
 * The followings are the available model relations:
 * @property Card $rF
 * @property Bike $bike
 * @property User $user
 */
class Bikerentral extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bikerentral';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('RFID, BikeID, StartTime, UserId', 'required'),
			array('RFID, BikeID, SrcStationID, DesStationID, Cost, UserId', 'numerical', 'integerOnly'=>true),
			array('StartTime, EndTiem', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('RFID, BikeID, SrcStationID, DesStationID, StartTime, EndTiem, Cost, BikeRentral, UserId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rF' => array(self::BELONGS_TO, 'Card', 'RFID'),
			'bike' => array(self::BELONGS_TO, 'Bike', 'BikeID'),
			'user' => array(self::BELONGS_TO, 'User', 'UserId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'RFID' => 'Rfid',
			'BikeID' => 'Bike',
			'SrcStationID' => 'Src Station',
			'DesStationID' => 'Des Station',
			'StartTime' => 'Start Time',
			'EndTiem' => 'End Tiem',
			'Cost' => 'Cost',
			'BikeRentral' => 'Bike Rentral',
			'UserId' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('RFID',$this->RFID);
		$criteria->compare('BikeID',$this->BikeID);
		$criteria->compare('SrcStationID',$this->SrcStationID);
		$criteria->compare('DesStationID',$this->DesStationID);
		$criteria->compare('StartTime',$this->StartTime,true);
		$criteria->compare('EndTiem',$this->EndTiem,true);
		$criteria->compare('Cost',$this->Cost);
		$criteria->compare('BikeRentral',$this->BikeRentral);
		$criteria->compare('UserId',$this->UserId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bikerentral the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
