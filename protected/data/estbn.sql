-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2014 at 09:20 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `estbn`
--

-- --------------------------------------------------------

--
-- Table structure for table `bike`
--

CREATE TABLE IF NOT EXISTS `bike` (
  `BID` int(11) NOT NULL AUTO_INCREMENT,
  `StationID` int(11) DEFAULT NULL,
  `serialNumber` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`BID`),
  UNIQUE KEY `serialNumber_UNIQUE` (`serialNumber`),
  KEY `fk_Bike_Station1_idx` (`StationID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bike`
--

INSERT INTO `bike` (`BID`, `StationID`, `serialNumber`) VALUES
(1, 1, '123456');

-- --------------------------------------------------------

--
-- Table structure for table `bikelocation`
--

CREATE TABLE IF NOT EXISTS `bikelocation` (
  `BikeID` int(11) NOT NULL,
  `lang` varchar(45) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `DateTime` varchar(50) NOT NULL,
  PRIMARY KEY (`BikeID`,`DateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bikerentral`
--

CREATE TABLE IF NOT EXISTS `bikerentral` (
  `RFID` int(11) NOT NULL,
  `BikeID` int(11) NOT NULL,
  `SrcStationID` int(11) DEFAULT NULL,
  `DesStationID` int(11) DEFAULT NULL,
  `StartTime` varchar(50) NOT NULL,
  `EndTiem` varchar(50) DEFAULT '0',
  `Cost` int(11) DEFAULT NULL,
  `BikeRentral` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  PRIMARY KEY (`BikeRentral`),
  KEY `fk_BikeRentral_Bike1_idx` (`BikeID`),
  KEY `fk_BikeRentral_Card1` (`RFID`),
  KEY `fk_BikeRentral_User1_idx` (`UserId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `bikerentral`
--

INSERT INTO `bikerentral` (`RFID`, `BikeID`, `SrcStationID`, `DesStationID`, `StartTime`, `EndTiem`, `Cost`, `BikeRentral`, `UserId`) VALUES
(1, 1, 1, 1, '1404854504', '1404854524', NULL, 1, 2),
(1, 1, 1, 1, '1404854599', '1404854918', NULL, 2, 2),
(1, 1, 1, NULL, '1404854932', '1404854932', NULL, 3, 2),
(1, 1, 1, NULL, '1404859643', '0', NULL, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `RFID` int(11) DEFAULT NULL,
  `Password` varchar(45) DEFAULT '123456',
  `CardId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`CardId`),
  UNIQUE KEY `RFID_UNIQUE` (`RFID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `card`
--

INSERT INTO `card` (`RFID`, `Password`, `CardId`) VALUES
(1, '123456', 2);

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `messageID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Message` text,
  PRIMARY KEY (`messageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`messageID`, `Name`, `Email`, `Message`) VALUES
(1, NULL, NULL, NULL),
(2, 'asdasd', 'qweqwe', '4123123123'),
(3, 'asdasd', 'qweqwe', '4123123123'),
(4, 'qqqqqq', 'rrrrrrrrrr', 'wwwwwwwww'),
(5, '123', '546', '678'),
(6, 'mohamed  adel', 'm.adel0093@gmail.com', 'hello i want to ask about ay haga :)'),
(7, NULL, NULL, NULL),
(8, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotles`
--

CREATE TABLE IF NOT EXISTS `hotles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `userName` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `hotles`
--

INSERT INTO `hotles` (`ID`, `Name`, `userName`, `password`) VALUES
(1, 'hotel1', 'm.adel0093', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `station`
--

CREATE TABLE IF NOT EXISTS `station` (
  `SID` int(11) NOT NULL AUTO_INCREMENT,
  `long` varchar(45) DEFAULT NULL,
  `lat` varchar(45) DEFAULT NULL,
  `Slots` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`SID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `station`
--

INSERT INTO `station` (`SID`, `long`, `lat`, `Slots`) VALUES
(1, '555555', '666666', '1111111');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `CardId` int(11) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL DEFAULT '0',
  `HotelId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_User_Card1_idx` (`CardId`),
  KEY `fk_User_Hotles1_idx` (`HotelId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `name`, `email`, `CardId`, `Active`, `HotelId`) VALUES
(1, 'amr ragab', 'amr@asd.asd', 2, 0, NULL),
(2, 'mohamed adel', 'm.adel0093@gmail.com', 2, 1, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bike`
--
ALTER TABLE `bike`
  ADD CONSTRAINT `fk_Bike_Station1` FOREIGN KEY (`StationID`) REFERENCES `station` (`SID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bikelocation`
--
ALTER TABLE `bikelocation`
  ADD CONSTRAINT `fk_BikeLocation_Bike1` FOREIGN KEY (`BikeID`) REFERENCES `bike` (`BID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bikerentral`
--
ALTER TABLE `bikerentral`
  ADD CONSTRAINT `fk_BikeRentral_Bike1` FOREIGN KEY (`BikeID`) REFERENCES `bike` (`BID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_BikeRentral_Card1` FOREIGN KEY (`RFID`) REFERENCES `card` (`RFID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_BikeRentral_User1` FOREIGN KEY (`UserId`) REFERENCES `user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_User_Card1` FOREIGN KEY (`CardId`) REFERENCES `card` (`CardId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_User_Hotles1` FOREIGN KEY (`HotelId`) REFERENCES `hotles` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
