<?php
class StationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','ShowStations','StationSlots','BikeOperation','GetBikes','GetHistory','SetInistate'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Station;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Station']))
		{
			$model->attributes=$_POST['Station'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->SID));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Station']))
		{
			$model->attributes=$_POST['Station'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->SID));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Station');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Station('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Station']))
			$model->attributes=$_GET['Station'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        public function actionShowStations()
        {
             $models=  Station::model()->findAll();
             foreach ($models as $model)
             {
                 echo $model->lat;
             }
        }
        public function actionStationSlots($id)
        {
            $station=$this->loadModel($id);
            if($station===null)
            {
                echo "FALSE";
            }else{
                echo $station->Slots;
            }
        }
        public function AbleToRent($RFID){
            $Rents = Yii::app()->db->createCommand("SELECT * FROM bikerentral WHERE RFID=$RFID AND EndTiem=0")->queryAll();
            
            if(count($Rents)==0)
            {               
                return true;
            }
            return false;
        }
        public function actionBikeOperation($RFID,$serialNumber,$StationId,$Slot,$Action){
            //Action=="return" || "take"
            $bike=bike::model()->find('serialNumber=?',array($serialNumber));
            $card=Yii::app()->db->createCommand("SELECT * FROM user AS u,card AS c WHERE u.CardId=c.CardId AND c.RFID=$RFID AND Active=1")->queryAll();
            $station=station::model()->find('SID=?',array($StationId));
            $Bikerentral=  new Bikerentral;          
            if($bike===null || $station===null)
            {
                echo "FALSE";
            }else{
                
                if($Action=="return")
                {
                    $criteria = new CDbCriteria;
                    $criteria->condition='RFID=:rfid AND BikeID=:bikeid AND EndTiem=:endtiem' ;
                    $criteria->params=array(':rfid'=>$RFID, ':bikeid'=>$bike->BID, ':endtiem'=>'0');
                    $Bikerentral=  Bikerentral::model()->find($criteria);
                    
                    if($Bikerentral===null)
                    {
                        echo "FALSE";
                        return;
                    }else{
                         echo "TRUE";
                    }
                    
                    $Slots=$station->Slots;
                    $Slots[$Slot]='1';
                    $station->Slots=$Slots;
                    $station->save();
                    $Bikerentral->DesStationID=$StationId;
                    $today = getdate();
                    $Bikerentral->EndTiem= $today[0];
                    $Bikerentral->save();
                    
                }else if($Action=="take" && $this->AbleToRent($RFID))
                {
                    
                    $Bikerentral=new Bikerentral();
                    
                    $Slots=$station->Slots;
                    $Slots[$Slot]='0';
                    $station->Slots=$Slots;
                    $station->save(); 
                    $Bikerentral->UserId=$card[0]['ID'];
                    $Bikerentral->RFID=$RFID;
                    $Bikerentral->BikeID=$bike->BID;
                    $Bikerentral->SrcStationID=$StationId;
                    $today = getdate();
                    $Bikerentral->StartTime= $today[0];
                    if($Bikerentral->save())
                    echo "TRUE"; 
                }else{
                    echo "FALSE";
                }
            }
            
        }
        public function actionGetHistory($RFID)
        {
            $card=Yii::app()->db->createCommand("SELECT * FROM user AS u,card AS c WHERE u.CardId=c.CardId AND c.RFID=$RFID AND Active=1")->queryAll();
            $userId=$card[0]['ID'];
            $hestory=Yii::app()->db->createCommand("SELECT * FROM bikerentral AS b WHERE UserId=$userId")->queryAll();
            $history=array();
            for($i=0;$i<count($hestory);$i++)
            {
                $history[$i]=new historyItems();
                $history[$i]->startDate=$hestory[$i]['StartTime'];
                $history[$i]->endDate=$hestory[$i]['EndTiem'];
                $history[$i]->Source=$hestory[$i]['SrcStationID'];
                $history[$i]->Dest=$hestory[$i]['DesStationID'];
                $history[$i]->Cost=$hestory[$i]['Cost'];
            }
            $a['Source']=$history;
            echo json_encode($a);
        }
        public function actionGetBikes($stationId){
            $model=Station::model()->findByPk($stationId);
            if($model!==null)
            {
                echo $model->Slots;
            }else{
                echo "FALSE";
            }
        }
        public function actionSetInistate($state,$stationId){
             $model=Station::model()->findByPk($stationId);
            if($model!==null)
            {
                $model->Slots=$state;
                $model->save();
            }else{
                echo "FALSE";
            }           
        }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Station the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Station::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Station $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='station-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
