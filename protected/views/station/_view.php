<?php
/* @var $this StationController */
/* @var $data Station */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('SID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->SID), array('view', 'id'=>$data->SID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('long')); ?>:</b>
	<?php echo CHtml::encode($data->long); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lat')); ?>:</b>
	<?php echo CHtml::encode($data->lat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Slots')); ?>:</b>
	<?php echo CHtml::encode($data->Slots); ?>
	<br />


</div>