<?php
/* @var $this CardController */
/* @var $data Card */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CardId')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CardId), array('view', 'id'=>$data->CardId)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RFID')); ?>:</b>
	<?php echo CHtml::encode($data->RFID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Password')); ?>:</b>
	<?php echo CHtml::encode($data->Password); ?>
	<br />


</div>