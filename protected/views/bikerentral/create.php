<?php
/* @var $this BikerentralController */
/* @var $model Bikerentral */

$this->breadcrumbs=array(
	'Bikerentrals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bikerentral', 'url'=>array('index')),
	array('label'=>'Manage Bikerentral', 'url'=>array('admin')),
);
?>

<h1>Create Bikerentral</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>