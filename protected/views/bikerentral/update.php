<?php
/* @var $this BikerentralController */
/* @var $model Bikerentral */

$this->breadcrumbs=array(
	'Bikerentrals'=>array('index'),
	$model->BikeRentral=>array('view','id'=>$model->BikeRentral),
	'Update',
);

$this->menu=array(
	array('label'=>'List Bikerentral', 'url'=>array('index')),
	array('label'=>'Create Bikerentral', 'url'=>array('create')),
	array('label'=>'View Bikerentral', 'url'=>array('view', 'id'=>$model->BikeRentral)),
	array('label'=>'Manage Bikerentral', 'url'=>array('admin')),
);
?>

<h1>Update Bikerentral <?php echo $model->BikeRentral; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>