<?php
/* @var $this BikerentralController */
/* @var $model Bikerentral */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bikerentral-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'RFID'); ?>
		<?php echo $form->textField($model,'RFID'); ?>
		<?php echo $form->error($model,'RFID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'BikeID'); ?>
		<?php echo $form->textField($model,'BikeID'); ?>
		<?php echo $form->error($model,'BikeID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'SrcStationID'); ?>
		<?php echo $form->textField($model,'SrcStationID'); ?>
		<?php echo $form->error($model,'SrcStationID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DesStationID'); ?>
		<?php echo $form->textField($model,'DesStationID'); ?>
		<?php echo $form->error($model,'DesStationID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'StartTime'); ?>
		<?php echo $form->textField($model,'StartTime',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'StartTime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'EndTiem'); ?>
		<?php echo $form->textField($model,'EndTiem',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'EndTiem'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Cost'); ?>
		<?php echo $form->textField($model,'Cost'); ?>
		<?php echo $form->error($model,'Cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UserId'); ?>
		<?php echo $form->textField($model,'UserId'); ?>
		<?php echo $form->error($model,'UserId'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->