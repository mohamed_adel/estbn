<?php
/* @var $this BikerentralController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bikerentrals',
);

$this->menu=array(
	array('label'=>'Create Bikerentral', 'url'=>array('create')),
	array('label'=>'Manage Bikerentral', 'url'=>array('admin')),
);
?>

<h1>Bikerentrals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
