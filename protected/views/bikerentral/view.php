<?php
/* @var $this BikerentralController */
/* @var $model Bikerentral */

$this->breadcrumbs=array(
	'Bikerentrals'=>array('index'),
	$model->BikeRentral,
);

$this->menu=array(
	array('label'=>'List Bikerentral', 'url'=>array('index')),
	array('label'=>'Create Bikerentral', 'url'=>array('create')),
	array('label'=>'Update Bikerentral', 'url'=>array('update', 'id'=>$model->BikeRentral)),
	array('label'=>'Delete Bikerentral', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->BikeRentral),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bikerentral', 'url'=>array('admin')),
);
?>

<h1>View Bikerentral #<?php echo $model->BikeRentral; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'RFID',
		'BikeID',
		'SrcStationID',
		'DesStationID',
		'StartTime',
		'EndTiem',
		'Cost',
		'BikeRentral',
		'UserId',
	),
)); ?>
