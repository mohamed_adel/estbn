<?php
/* @var $this BikerentralController */
/* @var $data Bikerentral */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('BikeRentral')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->BikeRentral), array('view', 'id'=>$data->BikeRentral)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RFID')); ?>:</b>
	<?php echo CHtml::encode($data->RFID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BikeID')); ?>:</b>
	<?php echo CHtml::encode($data->BikeID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('SrcStationID')); ?>:</b>
	<?php echo CHtml::encode($data->SrcStationID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DesStationID')); ?>:</b>
	<?php echo CHtml::encode($data->DesStationID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('StartTime')); ?>:</b>
	<?php echo CHtml::encode($data->StartTime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EndTiem')); ?>:</b>
	<?php echo CHtml::encode($data->EndTiem); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Cost')); ?>:</b>
	<?php echo CHtml::encode($data->Cost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UserId')); ?>:</b>
	<?php echo CHtml::encode($data->UserId); ?>
	<br />

	*/ ?>

</div>