<?php
/* @var $this BikerentralController */
/* @var $model Bikerentral */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'RFID'); ?>
		<?php echo $form->textField($model,'RFID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BikeID'); ?>
		<?php echo $form->textField($model,'BikeID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'SrcStationID'); ?>
		<?php echo $form->textField($model,'SrcStationID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DesStationID'); ?>
		<?php echo $form->textField($model,'DesStationID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'StartTime'); ?>
		<?php echo $form->textField($model,'StartTime',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'EndTiem'); ?>
		<?php echo $form->textField($model,'EndTiem',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Cost'); ?>
		<?php echo $form->textField($model,'Cost'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BikeRentral'); ?>
		<?php echo $form->textField($model,'BikeRentral'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'UserId'); ?>
		<?php echo $form->textField($model,'UserId'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->