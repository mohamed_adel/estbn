<?php
/* @var $this HotlesController */
/* @var $model Hotles */

$this->breadcrumbs=array(
	'Hotles'=>array('index'),
	$model->Name,
);

$this->menu=array(
	array('label'=>'List Hotles', 'url'=>array('index')),
	array('label'=>'Create Hotles', 'url'=>array('create')),
	array('label'=>'Update Hotles', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Hotles', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hotles', 'url'=>array('admin')),
);
?>

<h1>View Hotles #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'Name',
		'userName',
		'password',
	),
)); ?>
