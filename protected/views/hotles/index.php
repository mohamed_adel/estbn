<?php
/* @var $this HotlesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hotles',
);

$this->menu=array(
	array('label'=>'Create Hotles', 'url'=>array('create')),
	array('label'=>'Manage Hotles', 'url'=>array('admin')),
);
?>

<h1>Hotles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
