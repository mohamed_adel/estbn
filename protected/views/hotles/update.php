<?php
/* @var $this HotlesController */
/* @var $model Hotles */

$this->breadcrumbs=array(
	'Hotles'=>array('index'),
	$model->Name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hotles', 'url'=>array('index')),
	array('label'=>'Create Hotles', 'url'=>array('create')),
	array('label'=>'View Hotles', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Hotles', 'url'=>array('admin')),
);
?>

<h1>Update Hotles <?php echo $model->ID; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>