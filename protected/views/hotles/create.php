<?php
/* @var $this HotlesController */
/* @var $model Hotles */

$this->breadcrumbs=array(
	'Hotles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hotles', 'url'=>array('index')),
	array('label'=>'Manage Hotles', 'url'=>array('admin')),
);
?>

<h1>Create Hotles</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>