<?php
/* @var $this BikeController */
/* @var $model Bike */

$this->breadcrumbs=array(
	'Bikes'=>array('index'),
	$model->BID=>array('view','id'=>$model->BID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Bike', 'url'=>array('index')),
	array('label'=>'Create Bike', 'url'=>array('create')),
	array('label'=>'View Bike', 'url'=>array('view', 'id'=>$model->BID)),
	array('label'=>'Manage Bike', 'url'=>array('admin')),
);
?>

<h1>Update Bike <?php echo $model->BID; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>