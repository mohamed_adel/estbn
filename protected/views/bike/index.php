<?php
/* @var $this BikeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bikes',
);

$this->menu=array(
	array('label'=>'Create Bike', 'url'=>array('create')),
	array('label'=>'Manage Bike', 'url'=>array('admin')),
);
?>

<h1>Bikes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
