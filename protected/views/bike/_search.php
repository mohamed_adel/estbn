<?php
/* @var $this BikeController */
/* @var $model Bike */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'BID'); ?>
		<?php echo $form->textField($model,'BID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'StationID'); ?>
		<?php echo $form->textField($model,'StationID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'serialNumber'); ?>
		<?php echo $form->textField($model,'serialNumber',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->