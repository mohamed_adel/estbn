<?php
/* @var $this BikeController */
/* @var $model Bike */

$this->breadcrumbs=array(
	'Bikes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bike', 'url'=>array('index')),
	array('label'=>'Manage Bike', 'url'=>array('admin')),
);
?>

<h1>Create Bike</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>