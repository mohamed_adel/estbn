<?php
/* @var $this BikeController */
/* @var $data Bike */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('BID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->BID), array('view', 'id'=>$data->BID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('StationID')); ?>:</b>
	<?php echo CHtml::encode($data->StationID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serialNumber')); ?>:</b>
	<?php echo CHtml::encode($data->serialNumber); ?>
	<br />


</div>