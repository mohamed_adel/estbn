<?php
/* @var $this BikeController */
/* @var $model Bike */

$this->breadcrumbs=array(
	'Bikes'=>array('index'),
	$model->BID,
);

$this->menu=array(
	array('label'=>'List Bike', 'url'=>array('index')),
	array('label'=>'Create Bike', 'url'=>array('create')),
	array('label'=>'Update Bike', 'url'=>array('update', 'id'=>$model->BID)),
	array('label'=>'Delete Bike', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->BID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bike', 'url'=>array('admin')),
);
?>

<h1>View Bike #<?php echo $model->BID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'BID',
		'StationID',
		'serialNumber',
	),
)); ?>
