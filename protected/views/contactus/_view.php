<?php
/* @var $this ContactusController */
/* @var $data Contactus */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('messageID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->messageID), array('view', 'id'=>$data->messageID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email')); ?>:</b>
	<?php echo CHtml::encode($data->Email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Message')); ?>:</b>
	<?php echo CHtml::encode($data->Message); ?>
	<br />


</div>