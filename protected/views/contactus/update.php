<?php
/* @var $this ContactusController */
/* @var $model Contactus */

$this->breadcrumbs=array(
	'Contactuses'=>array('index'),
	$model->Name=>array('view','id'=>$model->messageID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Contactus', 'url'=>array('index')),
	array('label'=>'Create Contactus', 'url'=>array('create')),
	array('label'=>'View Contactus', 'url'=>array('view', 'id'=>$model->messageID)),
	array('label'=>'Manage Contactus', 'url'=>array('admin')),
);
?>

<h1>Update Contactus <?php echo $model->messageID; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>